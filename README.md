## Akwad

Akwad

#### License

MIT


POS Sales: 
When creating a POS sales invoice, the system adds the item to the invoice list after checking the stock availability, so:
1. If the item has stock, then the normal process goes.
2. If the item has no stock, then the following functionalities need to be customized:
3. The system should look for the same Item Name/code in the available Consignment Entries.
4. If there is a consignment entry that has the same item in it, the system should create a purchase invoice with that item as per the required quantity in the POS.
5. If there is more than one consignment entry with that item (either from the same supplier or from multiple suppliers), the system should pick the Consignment Entry with the least item rate. Then, it creates a Purchase Invoice (with update stock is enabled).
6. If the items in the POS order has more quantity than a single Consignment Entry, the system should go with more than Consignment Order.
7. If there is no quantity in the Consignment Entry, the system should not allow the purchase.
