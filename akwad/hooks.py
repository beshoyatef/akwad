from . import __version__ as app_version
# from akwad import akwad

app_name = "akwad"
app_title = "Akwad"
app_publisher = "Beshoy Atef"
app_description = "Akwad"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "beshoyatef31@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/akwad/css/akwad.css"
# app_include_js = "/assets/akwad/js/akwad.js"

# include js, css files in header of web template
# web_include_css = "/assets/akwad/css/akwad.css"
# web_include_js = "/assets/akwad/js/akwad.js"

# include custom scss in every website theme (without file extension ".scss")
# website_theme_scss = "akwad/public/scss/website"

# include js, css files in header of web form
# webform_include_js = {"doctype": "public/js/doctype.js"}
# webform_include_css = {"doctype": "public/css/doctype.css"}

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}


doctype_js = {
    'Sales Invoice': 'public/js/sales_invoice.js',
}
# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Generators
# ----------








# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "akwad.install.before_install"
# after_install = "akwad.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "akwad.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# DocType Class
# ---------------
# Override standard doctype classes



override_doctype_class = {
	"POS Invoice": "akwad.pos_invoice.POSInvoice"
}


doc_events = {
	"Journal Entry": {
		 "autoname":"akwad.custom.set_Journal_entry_date",
		
		
		
	},
	
	}
# override_whitelisted_methods = {
# 	"from erpnext.accounts.doctype.pos_invoice.pos_invoice.get_stock_availability": "akwad.pos_invoice.get_stock_availability2"
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"akwad.tasks.all"
# 	],
# 	"daily": [
# 		"akwad.tasks.daily"
# 	],
# 	"hourly": [
# 		"akwad.tasks.hourly"
# 	],
# 	"weekly": [
# 		"akwad.tasks.weekly"
# 	]
# 	"monthly": [
# 		"akwad.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "akwad.install.before_tests"

# Overriding Methods
# ------------------------------
#
override_whitelisted_methods = {
	"erpnext.accounts.doctype.pos_invoice.pos_invoice.get_stock_availability": "akwad.pos_invoice.get_stock_availability",
	"erpnext.selling.page.point_of_sale.point_of_sale.get_items" : "akwad.pos.get_items"
}
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "akwad.task.get_dashboard_data"
# }

# exempt linked doctypes from being automatically cancelled
#
# auto_cancel_exempted_doctypes = ["Auto Repeat"]


# User Data Protection
# --------------------

user_data_fields = [
	{
		"doctype": "{doctype_1}",
		"filter_by": "{filter_by}",
		"redact_fields": ["{field_1}", "{field_2}"],
		"partial": 1,
	},
	{
		"doctype": "{doctype_2}",
		"filter_by": "{filter_by}",
		"partial": 1,
	},
	{
		"doctype": "{doctype_3}",
		"strict": False,
	},
	{
		"doctype": "{doctype_4}"
	}
]

# Authentication and authorization
# --------------------------------

# auth_hooks = [
# 	"akwad.auth.validate"
# ]

