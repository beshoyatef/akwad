frappe.ui.form.on('Sales Invoice', {
    setup: function (frm) {
       frm.set_query("uom", "items", function (doc, cdt, cdn) {
         let row = locals[cdt][cdn];
         return {
           query:
             "erpnext.accounts.doctype.pricing_rule.pricing_rule.get_item_uoms",
           filters: {
             value: row.item_code,
             apply_on: "Item Code",
           },
         };
       });

       frm.set_query("purchase_price_list" , function (){
          return {
            filters: {
              "buying": 1
            }
          }
       });
      //  frm.set_query("purchase_", "items", function (doc, cdt, cdn) {
      //   let row = locals[cdt][cdn];
      //   return {
      //     query:
      //       "akwad.custom.custom_query",
      //     filters: {
      //       'item_code': row.item_code,
      //       'price_list':frm.doc.purchase_price_list,
      //       'customer' : frm.doc.customer , 
      //       apply_on: "Item Code",
      //     },
      //   };
      // });




     },
   
   
       
       
   })


   frappe.ui.form.on('Sales Invoice Item', {
    item_code(frm ,cdt,cdn ) {
      let row = locals[cdt][cdn];
        frappe.call({
          method  : 'akwad.custom.custom_query'  ,
          args:{
            'item_code': row.item_code,
            'price_list':frm.doc.purchase_price_list,
            'customer' : frm.doc.customer ,
            'date' :frm.doc.posting_date ,
          } ,
          callback:function(r){
            console.log(r)
            if(r.message){
              row.purchase = r.message
            }
            
          }
        })
    }
  })
   