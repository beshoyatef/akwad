import frappe 
from frappe.model.naming import getseries

@frappe.whitelist()
def set_Journal_entry_date(doc,*args,**kwars):

    if doc.voucher_type=='Bank Entry' and doc.payment_type=='Pay':
        d = str(doc.posting_date).split('-')
        perfex = 'BP-{}-'.format(str(d[1]))	
        doc.name =  perfex +getseries(perfex,4)
       


@frappe.whitelist()
def custom_query(*args ,**kwargs ):
    doc = kwargs
    item_code = doc.get('item_code')
    price_list = doc.get('price_list')
    date = doc.get('date')
    if item_code :
        qury = """ SELECT price_list_rate FROM `tabItem Price` WHERE  item_code = '{item_code}'
        and buying=1  and valid_upto >= '{date}' """.format(item_code=item_code , date=date) 
        if price_list :
            qury = qury + "AND price_list = '{price_list}' ".format(price_list=price_list)
        order_by = 'ORDER BY valid_from'
        qury = qury +order_by
        price = frappe.db.sql(qury,as_dict=True)
        p = price[0].get('price_list_rate')
        
        return p
    else :
        return 0