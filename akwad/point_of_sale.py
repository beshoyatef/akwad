import frappe
from frappe.model.mapper import get_mapped_doc
from frappe.utils import cstr, flt, cint
from erpnext.stock.doctype.item.item import get_item_defaults
from erpnext.setup.doctype.item_group.item_group import get_item_group_defaults
from erpnext.accounts.doctype.pos_profile.pos_profile import get_item_groups
#from .pos_invoice import get_stock_availability
def set_missing_values(source, target):
	target.ignore_pricing_rule = 1
	target.run_method("set_missing_values")
	target.run_method("calculate_taxes_and_totals")




def get_mapped_purchase_invoice(source_name, req_qty,item_code ,target_doc=None, ignore_permissions=False , ):
	def postprocess(source, target):
		target.flags.ignore_permissions = ignore_permissions
		set_missing_values(source, target)
		#Get the advance paid Journal Entries in Purchase Invoice Advance
		if target.get("allocate_advances_automatically"):
			target.set_advances()

		# target.set_payment_schedule()

	def update_item(obj, target, source_parent ):
		target.amount = flt(obj.amount) - flt(obj.billed_amt)
		target.base_amount = target.amount * flt(source_parent.conversion_rate)
		target.qty = target.amount / flt(obj.rate) if (flt(obj.rate) and flt(obj.billed_amt)) else flt(obj.qty)
		# target.qty = flt(re_qty)
		item = get_item_defaults(target.item_code, source_parent.company)
		item_group = get_item_group_defaults(target.item_code, source_parent.company)
		target.cost_center = (obj.cost_center
			or frappe.db.get_value("Project", obj.project, "cost_center")
			or item.get("buying_cost_center")
			or item_group.get("buying_cost_center"))

	fields = {
		"Purchase Order": {
			"doctype": "Purchase Invoice",
			"field_map": {
				"party_account_currency": "party_account_currency",
				"supplier_warehouse":"supplier_warehouse",
				
			},
			"field_no_map" : ["payment_terms_template"],
			"validation": {
				"docstatus": ["=", 1],
			}
		},
		"Purchase Order Item": {
			"doctype": "Purchase Invoice Item",
			"field_map": {
				"name": "po_detail",
				"parent": "purchase_order",
			},
			"postprocess": update_item,
			"condition": lambda doc: (doc.base_amount==0 or abs(doc.billed_amt) < abs(doc.amount))
		},
		"Purchase Taxes and Charges": {
			"doctype": "Purchase Taxes and Charges",
			"add_if_empty": True
		},
	}

	doc = get_mapped_doc("Purchase Order", source_name,	fields,
		target_doc, postprocess, ignore_permissions=ignore_permissions)

	for item in doc.items :
		if item.item_code == item_code:
			item.qty = req_qty
			item.received_qty = req_qty
		else :
			doc.remove(item)
	doc.save()
	doc.docstatus=1
	doc.update_stock =1
	doc.save()
	return


@frappe.whitelist()
def check_item_stock(item_code , wharehouse ):

    stock = frappe.db.sql(""" SELECT actual_qty  FROM `tabBin`  WHERE item_code = '%s'  and warehouse = '%s' """%( item_code , wharehouse))
    
  
    if stock :
        return str (stock[0][0] or '0')
    else :
        return "0"
    




@frappe.whitelist()
def get_rq_qty_from_purchase_order(item_code , qty ):
	ram_qty = qty 
	while ram_qty != 0 :
			purchase_order = frappe.db.sql( """  
            SELECT  `tabPurchase Order Item`.parent as parent  ,
                        (`tabPurchase Order Item`.stock_qty - `tabPurchase Order Item`.received_qty) as qty ,
                         `tabPurchase Order Item`.base_rate as min_price
            FROM `tabPurchase Order Item`
            INNER JOIN 
            `tabPurchase Order`
             ON `tabPurchase Order Item`.parent=`tabPurchase Order`.name
             WHERE `tabPurchase Order Item`.base_rate in 
                    (SELECT min(base_rate) FROM  `tabPurchase Order Item`  WHERE
                    (stock_qty - received_qty) > 0 and  item_code='%s') 
					and `tabPurchase Order`.docstatus =1 ;
			  """  %item_code ,as_dict=True)
			# frappe.msgprint(str(purchase_order))
			# order = frappe.get_doc('Purchase Order' , purchase_order[0].get('parent'))
			try :
				if float(ram_qty or 0 ) <=  float(purchase_order[0].get('qty') or  0):
					get_mapped_purchase_invoice( purchase_order[0].get('parent')  ,ram_qty , item_code)
					ram_qty = 0
				if float(ram_qty or 0 ) >  float(purchase_order[0].get('qty') or  0):
					get_mapped_purchase_invoice( purchase_order[0].get('parent')  ,float(purchase_order[0].get('qty') or  0) , item_code)
					ram_qty = float(ram_qty) - float(purchase_order[0].get('qty') or  0)
			
			except Exception as e:
				erro = frappe.new_doc("Error Log")
				erro.error =str(e)
				erro.save()
				break
@frappe.whitelist()
def get_qty_from_purchase_order(item_code ,wharehouse, qty ):
    #check available qty in purchase order 
    planned_qty = frappe.db.sql(""" SELECT projected_qty  FROM `tabBin`  WHERE item_code = '%s'  and warehouse = '%s' """%( item_code , wharehouse))
    if planned_qty :
        available_qty = float(planned_qty[0][0] or 0)
        if  float(available_qty or 0 ) < float(qty or 0 ) :
            frappe.throw("No enough qty ")
        
        if  float(available_qty or 0 ) > float(qty or 0 ) :
            #get item from purchase order to make 100
            
            get_rq_qty_from_purchase_order(item_code ,qty)
    else :
        frappe.throw("Un Aavilable Qty ")
@frappe.whitelist()
def validate_pos_invocie(doc , *args , **kwargs ):
	if doc.items :
		warehouse =doc.set_warehouse
		
		for item in doc.items :
            #check available stock
			item_stock = check_item_stock(item.item_code ,warehouse)
			
			#if not available
			
                #sheck purchase ordere for available purchase 
                # get the un stock qty 
				
			saled = frappe.db.sql(""" 
							select SUM(`tabPOS Invoice Item`.stock_qty) FROM `tabPOS Invoice Item` 
							LEFT JOIN `tabPOS Invoice` 
							ON `tabPOS Invoice Item`.parent = `tabPOS Invoice`.name 
							WHERE `tabPOS Invoice Item`.item_code = '%s' 
							and 
							`tabPOS Invoice`.set_warehouse = '%s' 
							and `tabPOS Invoice`.status='Paid' 
				 			and `tabPOS Invoice`.docstatus =1  """%(item.item_code , warehouse))
			item_stock = float(item_stock or 0 ) - float(saled[0][0] or  0)
			if float(item.qty) > float(item_stock) :
				
				req_qty=  float(item.qty or 0 ) - float(item_stock or 0) 

				get_qty_from_purchase_order(item.item_code , doc.set_warehouse,float(req_qty))
    



